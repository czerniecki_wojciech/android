package com.example.arkinis.skybox_dev;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ArkiNis on 2014-12-02.
 */
public class FileListAdapter extends BaseAdapter{

    private static final String IMAGE_URL_BASE = "http://covers.openlibrary.org/b/id/";

    Context mContext;
    LayoutInflater mInflater;
    public ArrayList<File_data> items_of_list = new ArrayList<File_data>();

    public FileListAdapter(Context context, LayoutInflater inflater) {
        mContext = context;
        mInflater = inflater;
    }

    @Override
    public int getCount() {
        return items_of_list.size();
    }

    @Override
    public Object getItem(int position) {
        return items_of_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.file_layout, null);

            holder = new ViewHolder();
            holder.thumbnailImageView = (ImageView) convertView.findViewById(R.id.img_thumbnail);
            holder.name = (TextView) convertView.findViewById(R.id.name_field);
            holder.size = (TextView) convertView.findViewById(R.id.size_field);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        // Get the item
        File_data fd = (File_data) getItem(position);

        holder.thumbnailImageView.setImageResource(R.drawable.fileicon);

        // Grab the title and author from the JSON
        String bookTitle = "";
        String authorName = "";

        bookTitle = fd.file_name;

        authorName = fd.file_size_mb.toString() + " MB";

        holder.name.setText(bookTitle);
        holder.size.setText(authorName);

        return convertView;
    }

    public void add_new(File_data file_data) {
        // update the adapter's dataset
        items_of_list.add(file_data);
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        public ImageView thumbnailImageView;
        public TextView name;
        public TextView size;
    }
}
