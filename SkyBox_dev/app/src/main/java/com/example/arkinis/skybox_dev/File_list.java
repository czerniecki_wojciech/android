package com.example.arkinis.skybox_dev;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class File_list extends Activity {
    ListView mainListView;
    FileListAdapter file_adapter;
    ArrayList mNameList = new ArrayList();

    private void Init()
    {
        mainListView = (ListView) findViewById(R.id.file_list);
        file_adapter = new FileListAdapter(this, getLayoutInflater());
        mainListView.setAdapter(file_adapter);

        file_adapter.add_new(new File_data("test0", 1.5f));
        file_adapter.add_new(new File_data("test1", 1.5f));
        file_adapter.add_new(new File_data("test2", 1.5f));
        file_adapter.add_new(new File_data("test3", 1.5f));
        file_adapter.add_new(new File_data("test3", 1.5f));
        file_adapter.add_new(new File_data("test4", 1.5f));
        file_adapter.add_new(new File_data("test5", 1.5f));
        file_adapter.add_new(new File_data("test6", 1.5f));
        file_adapter.add_new(new File_data("test7", 1.5f));
        file_adapter.add_new(new File_data("test8", 1.5f));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_list);

        Init();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_file_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
