package com.example.arkinis.skybox_dev;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class Create_account extends Activity {
    EditText login_input;
    EditText email_input;
    EditText password_input;
    EditText password_conf_input;

    Button create_account_button;
    Button reset_button;

    private void Init()
    {
        login_input = (EditText) findViewById(R.id.login_input);
        email_input = (EditText) findViewById(R.id.email_input);
        password_input = (EditText) findViewById(R.id.password_input);
        password_conf_input = (EditText) findViewById(R.id.password_conf_input);

        create_account_button = (Button) findViewById(R.id.create_acoount_button);
        create_account_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                create_acoount_function();
            }
        });

        reset_button = (Button) findViewById(R.id.reset_button);
        reset_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset_function();
            }
        });
    }

    private void create_acoount_function()
    {

    }

    private void reset_function()
    {
        login_input.setText("");
        email_input.setText("");
        password_input.setText("");
        password_conf_input.setText("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
