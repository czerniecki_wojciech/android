package com.example.arkinis.skybox_dev;

/**
 * Created by ArkiNis on 2014-12-02.
 */
public class File_data {
    public String file_name;
    public Float file_size_mb;

    File_data(String name, Float size)
    {
        file_name = name;
        file_size_mb = size;
    }
}
